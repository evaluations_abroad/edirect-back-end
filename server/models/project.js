'use strict';
var app = require('../../server/server');

module.exports = function(Project) {
  Project.remoteMethod('userProjects', {
    http: { path: '/userProjects', verb: 'get' },
    accepts: [
      { arg: "options", type: "object", http: "optionsFromRequest"},
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } }
    ],
    description: "Retrieve user projects",
    returns: { root: true, type: ["Project"]}
  });

  Project.userProjects = (options, req, res, next) => {
    new Promise(async (resolve, reject) => {
      try {
        const projects = await app.models.Project.find({
          where: {userId: options.accessToken.userId},
          include: ["tasks", "user"]
        });
        resolve(next(null, projects));
      } catch (error) {
        reject(next(error))
      }
    });
  }
};
