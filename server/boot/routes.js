'use strict';

module.exports = function(app) {
  app.get('/health', (req, res) => res.send({status: 'UP'}));
};
